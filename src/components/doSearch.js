import getItemHtml from "./getItemHtml";
import { showEmptyText, showExceededText } from "./resultsHandlers";

const doSearch = async (needle, projectId, token) => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    var oldResults = resultContainer.querySelectorAll('.dg_widget_result_item');
    oldResults.forEach(item => item.remove());

    document.querySelector('.dg_widget_spinner.dg_widget_hideable').style.display = 'block';
    resultContainer.style.display = 'flex';

    // check needle length before asking
    if (needle.length >= 50) {
        document.querySelector('.dg_widget_spinner.dg_widget_hideable').style.display = 'none';
        showExceededText();
        return
    }

    const api = `${process.env.api_url}/videos/search?search=${needle}&project_id=${projectId}`;
    
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    const options = {
        method: 'POST',
        headers: headers
    }

    const req = await fetch(api, options);
    const res = await req.json();
    let results = res.results;

    document.querySelector('.dg_widget_spinner.dg_widget_hideable').style.display = 'none';

    if(typeof results != 'undefined'){
        var result_exact = [];
        var result_fuzzy = [];
        results.map(item => {
            item.exact ? result_exact.push(item) : result_fuzzy.push(item);
        });
        result_exact.length > 0 && result_exact.map(async (item) => {
            var temp = await getItemHtml(item, needle);
            resultContainer.insertAdjacentHTML('beforeend', temp);
        });
        result_fuzzy.length > 0 && result_fuzzy.map(async (item) => {
            var temp = await getItemHtml(item, needle);
            resultContainer.insertAdjacentHTML('beforeend', temp);
        });

        if (results.length <= 0) {
            showEmptyText();
        }
    } else {
        results = [];
    }
}

export default doSearch;